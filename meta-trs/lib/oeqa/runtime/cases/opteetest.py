#
# SPDX-License-Identifier: MIT
#

import os

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.runtime.decorator.package import OEHasPackage

class OpteeTestSuite(OERuntimeTestCase):
    """
    Run OP-TEE tests (xtest).
    """
    @OEHasPackage(['optee-test'])
    def test_opteetest_xtest(self):
        cmd = "xtest"
        status, output = self.target.run(cmd, 600)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
